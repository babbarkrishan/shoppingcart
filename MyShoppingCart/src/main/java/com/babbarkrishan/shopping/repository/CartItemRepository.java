
package com.babbarkrishan.shopping.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.babbarkrishan.shopping.entity.CartItem;

public interface CartItemRepository extends MongoRepository<CartItem, String> {

}
