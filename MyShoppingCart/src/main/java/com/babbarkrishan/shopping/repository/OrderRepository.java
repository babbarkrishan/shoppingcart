
package com.babbarkrishan.shopping.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.babbarkrishan.shopping.entity.Order;

public interface OrderRepository extends MongoRepository<Order, String> {

}
