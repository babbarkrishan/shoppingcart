package com.babbarkrishan.shopping.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.babbarkrishan.shopping.entity.Product;
import com.babbarkrishan.shopping.exception.ProductNotFoundException;
import com.babbarkrishan.shopping.repository.ProductRepository;

@Service
final class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public Product create(Product product) {		
		return productRepository.save(product);
		
	}

	@Override
	public List<Product> findAll() {
		List<Product> productEntries = productRepository.findAll();
		return productEntries;
	}

	@Override
	public Product findById(String id) throws ProductNotFoundException {
		Product found = findProductById(id);
		return found;
	}

	@Override
	public Product update(Product product) throws ProductNotFoundException {
		Product updated = findProductById(product.getId().toString());
		updated.setProductName(product.getProductName());
		updated.setPrice(product.getPrice());
		updated.setDescription(product.getDescription());
		updated.setImagePath(product.getImagePath());
		updated = productRepository.save(updated);
		return updated;
	}

	private Product findProductById(String id) throws ProductNotFoundException {
		Product result = productRepository.findOne(id);
		return result;
	}
}