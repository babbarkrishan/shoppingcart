package com.babbarkrishan.shopping.service;

import java.util.List;

import com.babbarkrishan.shopping.bean.UserSearchCriteria;
import com.babbarkrishan.shopping.entity.User;
import com.babbarkrishan.shopping.exception.UserNotFoundException;

public interface UserService {
	User create(User user);

	List<User> findAll();

	User findById(String id) throws UserNotFoundException;
	
	List<User> search(UserSearchCriteria userSearchCriteria) throws UserNotFoundException;

	User update(User user) throws UserNotFoundException;
}
