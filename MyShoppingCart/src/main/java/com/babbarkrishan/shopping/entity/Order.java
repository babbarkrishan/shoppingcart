package com.babbarkrishan.shopping.entity;


import java.util.List;

import org.springframework.data.annotation.Id;

import com.babbarkrishan.shopping.Constants.OrderStatus;
import com.babbarkrishan.shopping.Constants.PaymentMode;
import com.babbarkrishan.shopping.Constants.PaymentStatus;

public class Order {
	@Id
	private String id;

	private List<CartItem> cartItems;
	private float totalAmount;
	private OrderStatus orderStatus;
	private PaymentMode paymentMode;
	private PaymentStatus paymentStatus;
	private Address deliveryAddress;

	public Order() {		
	}	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<CartItem> getCartItems() {
		return cartItems;
	}

	public void setCartItems(List<CartItem> cartItems) {
		this.cartItems = cartItems;
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public PaymentMode getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(PaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Address getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	@Override
	public String toString() {
		return "Product [Id=" + id + ", cartItems=" + cartItems.toString() + ", totalAmount=" + totalAmount
				+ ", orderStatus=" + orderStatus + ", paymentMode=" + paymentMode + ", paymentStatus=" + paymentStatus + ", deliveryAddress=" + deliveryAddress + "]";
	}	
}