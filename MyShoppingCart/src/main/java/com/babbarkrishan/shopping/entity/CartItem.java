
package com.babbarkrishan.shopping.entity;


import org.springframework.data.annotation.Id;

public class CartItem {
	
	@Id
	private String id;

	private Product product;
	private int quantity;
	private float amount;	

	public CartItem() {		
	}
	
	public CartItem(Product product, int quantity, float amount) {
		this.product = product;
		this.quantity = quantity;
		this.amount= amount; 
	}
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Cart Item [Id=" + id + ", product=" + product.toString() + ", quantity=" + quantity
				+ ", amount=" + amount + "]";
	}
}