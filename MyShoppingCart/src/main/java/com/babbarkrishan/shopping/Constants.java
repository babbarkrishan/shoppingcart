package com.babbarkrishan.shopping;

public class Constants {
	public enum OrderStatus {
		PENDING("pending"), CONFIRMED("confirmed"), SHIPPED("shipped"), DELIVERED("delivered");		
		
		private final String value;
		private OrderStatus(String value) {
			this.value = value;
		}
		
		public String getValue() {
			return value;
		}
		
		public static OrderStatus getOrderStatusByValue(String value) {
			 for (OrderStatus orderStatus : OrderStatus.values()) {
			      if (orderStatus.value.equalsIgnoreCase(value)) {
			        return orderStatus;
			      }
			    }
			    return null;
		}
	}
	
	public enum PaymentMode {
		COD("cod"), CREDITCARD("creditcard"), DEBITCARD("debitcard");		
		
		private final String value;
		private PaymentMode(String value) {
			this.value = value;
		}
		
		public String getValue() {
			return value;
		}
		
		public static PaymentMode getPaymentModeByValue(String value) {
			 for (PaymentMode paymentMode : PaymentMode.values()) {
			      if (paymentMode.value.equalsIgnoreCase(value)) {
			        return paymentMode;
			      }
			    }
			    return null;
		}
	}
	
	public enum PaymentStatus {
		PENDING("pending"), INPROCESS("inprocess"), DONE("done");		
		
		private final String value;
		private PaymentStatus(String value) {
			this.value = value;
		}
		
		public String getValue() {
			return value;
		}
		
		public static PaymentStatus getPaymentStatusByValue(String value) {
			 for (PaymentStatus paymentStatus : PaymentStatus.values()) {
			      if (paymentStatus.value.equalsIgnoreCase(value)) {
			        return paymentStatus;
			      }
			    }
			    return null;
		}
	}
	
	public enum AddressType {
		BILLING("billing"), DELIVERY("delivery"), OFFICIAL("official");		
		
		private final String value;
		private AddressType(String value) {
			this.value = value;
		}
		
		public String getValue() {
			return value;
		}
		
		public static AddressType getAddressTypeByValue(String value) {
			 for (AddressType addressType : AddressType.values()) {
			      if (addressType.value.equalsIgnoreCase(value)) {
			        return addressType;
			      }
			    }
			    return null;
		}
	}
	
}
