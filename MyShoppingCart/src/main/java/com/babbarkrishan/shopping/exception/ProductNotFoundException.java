package com.babbarkrishan.shopping.exception;

public class ProductNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProductNotFoundException(String id) {
		super("Product with id " + id + " not found.");
	}
}
