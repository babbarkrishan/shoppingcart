# README #
Build a shopping cart with:
1.
Angularjs/Bootstrap as the UI layer deployed on expressjs

2.
REST layer in Spring 
Deployed as Spring Boot

3.
Backend DB as mongoDB

### What is this repository for? ###

* A sample project having a small shopping cart using Mongo DB at backend.
* UI is developed using Angular and Bootstrap and deployed in ExpressJS

### Pre-requisite ###
* Java 8 (May run on older version, but i tested on Java 8 only.)
* Mongo DB. I used MongoDB server version: 3.4.7

### How do I get set up? ###

* Run Mongo DB server
* Change the DB details in application.properties, if require
* REST APIs project is build in SpringBoot
* When you do maven install, it will create jar in target folder. 
* Deploy this jar file where you want and execute the same using following command.
	java -jar <jar_file_name>

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Krishan Babbar <krishan.babbar@gmail.com>
